from flask import Flask, render_template, request, make_response, jsonify
import json
import requests
import hashlib
from time import time
from datetime import datetime
import random
import copy
import math

# import boto3
# from botocore.exceptions import NoCredentialsError

import firebase_admin
from firebase_admin import credentials, db, auth

from uuid import uuid4


SMS_KEY = "UHGo1hNE4C8MjfvQVy9wPdzZkJcxsaDnRpFLX367SYrAWgeq5OjHsWDmqeKZ9w4oP6LuyvIETXr7Clk2"
# ACCESS_KEY = 'AKIAJY3LAUEGVYYVWACA'
# SECRET_KEY = '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'

# s3 = boto3.resource('s3', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)

# def put( folder, file, data ):
#     try:
#         object = s3.Object('nishanth123', folder+'/'+file+'.json')
#         return object.put(Body=json.dumps(data).encode('utf-8'))
#     except:
#         raise

# def get( folder, file ):
#     try:
#         object = s3.Object('nishanth123', folder+'/'+file+'.json')
#         return json.loads(object.get()['Body'].read().decode('utf-8'))
#     except:
#         raise

# print(put( 'nish' , 'file' , {'name':'nishanth', 'place':'tumkur'}))
# print(get('nish' , 'file' ))

# # Method 1: Object.put()
# s3 = boto3.resource('s3', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
# object = s3.Object('nishanth123', 'filename.txt')
# object.put(Body=some_binary_data)

# # Method 2: Client.put_object()
# client = boto3.client('s3', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
# client.put_object(Body=more_binary_data, Bucket='nishanth123', Key='anotherfilename.txt')

# obj = s3.Object('nishanth123', 'anotherfilename.txt')
# body = obj.get()['Body'].read()
# print(body)

cred = credentials.Certificate('serviceAccountKey.json')
default_app = firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://tracker-india.firebaseio.com'
})

app = Flask(__name__)

def custom_hash(block):
    block_string = json.dumps(block, sort_keys=True).encode()
    return hashlib.sha256(block_string).hexdigest()

def valid_proof(last_proof, proof):

    """
    Validates the Proof: Does hash(last_proof, proof) contain 4 leading zeroes?
    :param last_proof: <int> Previous Proof
    :param proof: <int> Current Proof
    :return: <bool> True if correct, False if not.
    """
    try:
        guess = f'{last_proof}{proof}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:4] == "0000"
    except:
        raise

def proof_of_work(last_proof):
    """
    Simple Proof of Work Algorithm:
     - Find a number p' such that hash(pp') contains leading 4 zeroes, where p is the previous p'
     - p is the previous proof, and p' is the new proof
    :param last_proof: <int>
    :return: <int>
    """
    try:
        proof = 0
        while valid_proof(last_proof, proof) is False:
            proof += 1

        return proof
    except:
        raise

def get_balance(checkpoint, blockchain = None):
    try:
        if blockchain:
            coins = 0
            for block in blockchain:
                if 'reciever' in block and block['reciever'] == checkpoint:
                    coins += block['value']
                if 'sender' in block and block['sender'] == checkpoint:
                    coins -= block['value']
            return coins
        else:
            lost_blocks = db.reference('/blockchains/'+checkpoint).order_by_child('sender').equal_to(checkpoint).get()
            gained_blocks = db.reference('/blockchains/'+checkpoint).order_by_child('reciever').equal_to(checkpoint).get()
            return ( sum(list(map(lambda a: a['value'], gained_blocks.values()))) - sum(list(map(lambda a: a['value'], lost_blocks.values()))) )
    except:
        raise

def add_transaction(initiator, transaction):
    try:
        checkpoints = db.reference('/network/users').get()
        if checkpoints[initiator]['admin']:
            initiator = random.choice(list(checkpoints.keys()))
            while checkpoints[initiator]['admin']:
                initiator = random.choice(list(checkpoints.keys()))
        lastblock = list(db.reference('/blockchains/'+initiator).order_by_key().limit_to_last(1).get().items())[0][1]
        if transaction['sender']!="0" and 'value' in transaction:
            coins = get_balance(checkpoint=transaction['sender'])
            if coins < transaction['value']:
                return 'Insufficient Balance Found'
        if lastblock == None:
            index = 0
            previous_hash = 1
            proof = 100
        else:
            index = lastblock['index'] + 1
            previous_hash = custom_hash(lastblock)
            last_proof = lastblock['proof']
            proof = proof_of_work(last_proof)
        block = {
            'index': index,
            'timestamp': time(),
            'proof': proof,
            'previous_hash': previous_hash
        }
        for entry in transaction:
            block[entry] = transaction[entry]
        for checkpoint in checkpoints:
            if not checkpoints[checkpoint]['admin']:
                db.reference('/blockchains/'+checkpoint+'/'+str(index)).set(block)
        return block
    except:
        raise

def find_path(source, destination, detailed=False):
    try:
        checkpoints = db.reference('/network/users').get()
        edges = db.reference('/network/connections').get()
        cost = {}
        onedcost = {}
        for checkpoint in checkpoints:
            onedcost[checkpoint] = 999999
        for checkpoint in checkpoints:
            cost[checkpoint] = copy.copy(onedcost)
            checkpoints[checkpoint]['visited'] = False
            cost[checkpoint][checkpoint] = 0
        # print(cost)
        for edge in edges:
            cost[edges[edge]['from']][edges[edge]['to']] = edges[edge]['distance']
            cost[edges[edge]['to']][edges[edge]['from']] = edges[edge]['distance']
        # print(cost)
        checkpoints[source]['parent'] = source
        checkpoints[source]['visited'] = True
        for checkpoint in checkpoints:
            if cost[source][checkpoint] == 999999:
                checkpoints[checkpoint]['distance'] = 999999
            else:
                checkpoints[checkpoint]['distance'] = cost[source][checkpoint]
                checkpoints[checkpoint]['parent'] = source
        for i in checkpoints:
            nearest_checkpoint = ''
            minDist = 999999
            for checkpoint in checkpoints:
                if ( (not checkpoints[checkpoint]['visited']) and (minDist >= checkpoints[checkpoint]['distance']) ):
                    nearest_checkpoint = checkpoint
                    minDist = checkpoints[checkpoint]['distance']
            if nearest_checkpoint == destination:
                path = []
                node = destination
                path.append(destination)
                while node!=source:
                    path.append(checkpoints[node]['parent'])
                    node = checkpoints[node]['parent']
                path.reverse()
                if detailed:
                    track_details={}
                    for checkpoint in path:
                        track_details[checkpoint] = {
                            'distance_to_source':checkpoints[checkpoint]['distance'],
                            'distance_to_previous_checkpoint': checkpoints[checkpoint]['distance']-checkpoints[checkpoints[checkpoint]['parent']]['distance'],
                            'message':[],
                            'type': 'checkpoint',
                            'position': math.floor(checkpoints[checkpoint]['distance']*100/checkpoints[destination]['distance']),
                            'previous_checkpoint': checkpoints[checkpoint]['parent']
                        }
                    return {'path':path,'distance':checkpoints[destination]['distance'],'track_details':track_details}
                return {'path':path,'distance':checkpoints[destination]['distance']}
            if nearest_checkpoint == '':
                return f'No path found from {checkpoints[source]["place"]} to {checkpoints[destination]["place"]}'
            checkpoints[nearest_checkpoint]['visited'] = True
            for checkpoint in checkpoints:
                if cost[nearest_checkpoint][checkpoint] != 999999:
                    if checkpoints[checkpoint]['distance'] > checkpoints[nearest_checkpoint]['distance']+cost[nearest_checkpoint][checkpoint]:
                        checkpoints[checkpoint]['distance'] = checkpoints[nearest_checkpoint]['distance']+cost[nearest_checkpoint][checkpoint]
                        checkpoints[checkpoint]['parent'] = nearest_checkpoint
                    

    except:
        raise

# blockchain = db.reference('/blockchains/mojudF6SUmRY8QwRGFmN3hAmX0i1').get()
# print(get_balance('5MG8AVMN9qZVxeTx8uR3G8d7cK12', blockchain=blockchain))
# print(get_balance('S2YUjp6VXrZrLePx8Q1SXnx2Rrc2', blockchain=blockchain))
# print(get_balance('UEalgahIk1TUS9J4ZylZN7pLtg12', blockchain=blockchain))
# print(get_balance('Y6YrhquvOTQ3lYCWCYMqErskXKs1', blockchain=blockchain))
# print(get_balance('jlxFPDo3bggplCIl0GOruw4sJnH3', blockchain=blockchain))
# print(get_balance('mojudF6SUmRY8QwRGFmN3hAmX0i1', blockchain=blockchain))

@app.route('/checkpoints', methods=['PUT', 'POST', 'DELETE'])
def checkpoints():
    try:
        if request.method == 'POST':
            user = auth.create_user(
                email=request.json['email'],
                password='nish4anth',
            )
            data = request.json
            data['admin'] = False
            db.reference('/network/users/'+user.uid).set(data)
            return make_response(jsonify({'message': 'New Checkpoint Added', 'code': 200}), 200)
        if request.method == 'PUT':
            data = request.json
            uid = data['uid']
            del data['uid']
            db.reference('/network/users/'+uid).set(data)
            return make_response(jsonify({'message': 'Checkpoint Updated', 'code': 200}), 200)
        if request.method == 'DELETE':
            data = request.json
            uid = data['uid']
            auth.delete_user(uid)
            cons = db.reference('/network/connections').get()
            for con in cons:
                if ( cons[con]['from'] == uid or cons[con]['from'] == uid ):
                    db.reference('/network/connections/'+con).set({})
            db.reference('/network/users/'+uid).set({})
            return make_response(jsonify({'message': 'Checkpoint Deleted', 'code': 200}), 200)
    except Exception as e:
        print(str(repr(e)))
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/connections', methods=['POST', 'DELETE'])
def connections():
    try:
        if request.method == 'POST':
            if request.json['node1'] == request.json['node2']:
                return make_response(jsonify({'message': 'Same checkpoint cannot be connected', 'code': 400}), 400)
            if request.json['distance'] == 0:
                return make_response(jsonify({'message': 'Distance cannot be 0', 'code': 400}), 400)
            connections = db.reference('/network/connections').get()
            if connections != None:
                for con in connections:
                    if ( connections[con]['from'] == request.json['node1'] and connections[con]['to'] == request.json['node2'] or connections[con]['from'] == request.json['node2'] and connections[con]['to'] == request.json['node1'] ):
                        return make_response(jsonify({'message': 'These Checkpoints are already connected', 'code': 400}), 400)
            db.reference('/network/connections').push({ 'from':request.json['node1'], 'to':request.json['node2'], 'distance':request.json['distance'] })
            return make_response(jsonify({'message': 'These Checkpoints now connected', 'code': 200}), 200)
        if request.method == 'DELETE':
            db.reference('/network/connections/'+request.json['edge']).set({})
            return make_response(jsonify({'message': 'These Checkpoints now now disconnected', 'code': 200}), 200)
    except Exception as e:
        print(e)
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/check_coins/<uid>', methods=['GET'])
def check_coins(uid):
    try:
        return make_response(jsonify({'message': f'This Checkpoint has {get_balance(uid)} coins', 'code': 200}), 200)
    except Exception as e:
        print(e)
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/add_coins', methods=['POST'])
def add_coins():
    try:
        uid = request.cookies.get("uid")
        if uid == None:
            return make_response(jsonify({'message': 'You are not a part of this network', 'code': 400}), 400)
        res = add_transaction(
            initiator = uid,
            transaction = {
                'sender': "0",
                'reciever': request.json['reciever'],
                'value': int(request.json['value'])
            }
        )
        if type( res ) == str :
            return make_response(jsonify({'message': res, 'code': 400}), 400)
        return make_response(jsonify({'message': str(request.json['value'])+' is given to '+str(request.json['reciever']), 'code': 200}), 200)
    except Exception as e:
        print(e)
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/remove_coins', methods=['POST'])
def remove_coins():
    try:
        uid = request.cookies.get("uid")
        if uid == None:
            return make_response(jsonify({'message': 'You are not a part of this network', 'code': 400}), 400)
        res = add_transaction(
            initiator = uid,
            transaction = {
                'sender': request.json['sender'],
                'reciever': "0",
                'value': int(request.json['value'])
            }
        )
        if type( res ) == str :
            return make_response(jsonify({'message': res, 'code': 400}), 400)
        return make_response(jsonify({'message': str(request.json['value'])+' is taken from '+str(request.json['sender']), 'code': 200}), 200)
    except Exception as e:
        print(e)
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

def get_cost_to_customer(distance, weight):
    return math.floor(weight*((distance*0.02)+5)+25)

def get_cost_to_ship(distance, weight):
    return math.floor(weight*((distance*0.02)+5)+10)

def get_cost_to_deliver(weight):
    return math.floor(weight*5+10)

def send_sms(message, to):
    try:
        url = "https://www.fast2sms.com/dev/bulk"

        querystring = {"authorization": SMS_KEY,"sender_id":"FSTSMS","message":message,"language":"english","route":"p","numbers":to}

        headers = {
            'cache-control': "no-cache"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        return response["return"]
    except:
        return False

@app.route('/register_package', methods=['POST'])
def register_package():
    try:
        uid = request.cookies.get("uid")
        if uid == None:
            return make_response(jsonify({'message': 'You are not a part of this network', 'code': 400}), 400)
        package_id = str(uuid4()).replace('-', '')
        path = find_path(source=uid, destination=request.json['destination'])
        if type(path) == str :
            return make_response(jsonify({'message': path, 'code': 400}), 400)
        checkpoints = db.reference('/network/users').get()
        distance = path['distance']
        path = (list(map(lambda a: checkpoints[a]['place'], path['path'])))
        cost_to_customer = get_cost_to_customer(distance=distance, weight=int(request.json['weight']))
        pin = str(random.randint(1111,9999))
        res = add_transaction(
            initiator = uid, 
            transaction = {
                'sender': uid,
                'reciever': "0",
                'value': int(request.json['value']),
                'type': 'REGISTER_PACKAGE',
                'package_id': package_id,
                'destination': request.json['destination'],
                'owner_name': request.json['owner_name'],
                'mob': request.json['mob'],
                'weight': request.json['weight'],
                'cost_to_customer': cost_to_customer,
                'pin': pin
            }
        )
        print(path)
        if type( res )== str :
            return make_response(jsonify({'message':res, 'code': 400}), 400)
        res['path'] = path
        res['distance'] = distance
        send_sms( to=request.json['mob'] , message=f'''
Dear {request.json["owner_name"]},
Your PackageId: {res["package_id"]},
Your Pin: {res["pin"]}
Share it only with the delivery agent after receiving the package successfully.
Thank you for using our tracking system.''')
        return make_response(jsonify({'message':'Package registered Successfully', 'package_details':res, 'code': 200}), 200)
    except Exception as e:
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/packages_to_be_forwarded', methods = ['GET'])
def packages_to_be_forwarded():
    try:
        uid = request.cookies.get("uid")
        if uid == None:
            return make_response(jsonify({'message': 'You are not a part of this network', 'code': 400}), 400)
        ref = db.reference('/blockchains/'+uid)
        blockchain = ref.get()

        in_packages = list(map( lambda trans: trans['package_id'], list(filter( lambda trans: 'type' in trans and trans['sender'] == uid and (trans['type'] == 'REGISTER_PACKAGE' or trans['type'] == 'RECIEVE_PACKAGE'), blockchain))))
        out_packages = list(map( lambda trans: trans['package_id'], list(filter( lambda trans: 'type' in trans and trans['reciever'] == uid and (trans['type'] == 'FORWARD_PACKAGE' or trans['type'] == 'DELIVER_PACKAGE'), blockchain))))
        
        packages = copy.copy(in_packages)
        
        for package in out_packages:
            packages.remove(package)
        
        packages = list(map(lambda package: list(filter(lambda trans: 'type' in trans and trans['type'] == 'REGISTER_PACKAGE' and trans['package_id'] == package, blockchain))[0], packages))
        packages = list(filter(lambda package: package['destination'] != uid, packages))
        for package in packages:
            package['next_checkpoint'] = db.reference('/network/users/'+find_path(source=uid, destination=package['destination'])['path'][1]).get()['place']
            package['destination'] = db.reference('/network/users/'+package['destination']).get()['place']
        return make_response(jsonify({'message': {'packages':packages}, 'code': 200}), 200)
    except Exception as e:
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)


@app.route('/forward_package', methods=['POST'])
def forward_package():
    try:
        uid = request.cookies.get("uid")
        if uid == None:
            return make_response(jsonify({'message': 'You are not a part of this network', 'code': 400}), 400)
        ref = db.reference('/blockchains/'+uid)
        transactions = list(ref.order_by_child('package_id').equal_to(request.json['package_id']).get().values())
        if len(transactions) == 0:
            return make_response(jsonify({'message': 'Package not found', 'code': 400}), 400)
        transactions = sorted(transactions, key=lambda a: a['index'])
        last_transaction = transactions[-1]
        if not (last_transaction['sender'] == uid and (last_transaction['type'] == 'REGISTER_PACKAGE' or last_transaction['type'] == 'RECIEVE_PACKAGE')):
            return make_response(jsonify({'message': 'You cant Forward this Package because you are not supposed to have this package', 'code': 400}), 400)
        package_details = list(filter(lambda a: a['type'] == 'REGISTER_PACKAGE', transactions))[0]
        if uid == package_details['destination']:
            return make_response(jsonify({'message': 'You cant forward this package because you are the destination of this package. You can only deliver this package.', 'code': 400}), 400)
        path = find_path(source=uid, destination=package_details['destination'])['path']
        next_checkpoint = path[1]
        res = add_transaction(
            initiator = uid, 
            transaction = {
                'sender': "0",
                'reciever': uid,
                'value': 0,
                'type': 'FORWARD_PACKAGE',
                'package_id': package_details['package_id'],
                'next_checkpoint': next_checkpoint
            }
        )
        if type( res )== str :
            return make_response(jsonify({'message': res, 'code': 400}), 400)
        package_details['destination'] = db.reference('/network/users/'+package_details['destination']).get()['place']
        checkpoints = db.reference('/network/users').get()
        package_details['path'] = (list(map(lambda a: checkpoints[a]['place'], path)))
        return make_response(jsonify({'message': f"Take this package to {db.reference('/network/users/'+next_checkpoint).get()['place']}", "package": package_details, 'code': 200}), 200)
    except Exception as e:
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/forwarded_packages', methods = ['GET'])
def forwarded_packages():
    try:
        uid = request.cookies.get("uid")
        if uid == None:
            return make_response(jsonify({'message': 'You are not a part of this network', 'code': 400}), 400)
        ref = db.reference('/blockchains/'+uid)
        blockchain = ref.get()
        blockchain = sorted(blockchain, key=lambda a: a['index'])
        forwarded_packages = list(map( lambda trans: trans['package_id'], list(filter( lambda trans: 'type' in trans and trans['reciever'] == uid and (trans['type'] == 'FORWARD_PACKAGE'), blockchain))))
        
        def func1(package):
            last_trans = list(filter(lambda trans: 'package_id' in trans and trans['package_id'] == package, blockchain))[-1]
            return (last_trans['type'] == 'FORWARD_PACKAGE' and last_trans['reciever'] == uid)
        
        packages = list(filter( func1 , forwarded_packages))
        

        packages = list(map(lambda package: list(filter(lambda trans: 'type' in trans and trans['type'] == 'REGISTER_PACKAGE' and trans['package_id'] == package, blockchain))[0], packages))
        
        for package in packages:
            package['next_checkpoint'] = db.reference('/network/users/'+find_path(source=uid, destination=package['destination'])['path'][1]).get()['place']
            package['destination'] = db.reference('/network/users/'+package['destination']).get()['place']
        return make_response(jsonify({'message': {'packages':packages}, 'code': 200}), 200)
    except Exception as e:
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/recieve_package', methods = ['POST'])
def recieve_package():
    try:
        uid = request.cookies.get("uid")
        if uid == None:
            return make_response(jsonify({'message': 'You are not a part of this network', 'code': 400}), 400)
        transactions = list(db.reference('/blockchains/'+uid).order_by_child('package_id').equal_to(request.json['package_id']).get().values())
        if transactions == []:
            return make_response(jsonify({'message': 'Package not found', 'code': 400}), 400)
        transactions = sorted(transactions, key=lambda a: a['index'])
        last_trans = transactions[-1]
        package_details = list(filter(lambda trans: trans['type'] == 'REGISTER_PACKAGE', transactions))[0] 
        if not (last_trans['type'] == 'FORWARD_PACKAGE' and last_trans['next_checkpoint'] == uid):
            return make_response(jsonify({'message': 'You are not supposed to recieve this package because it is not forwarded to you', 'code': 400}), 400)
        res = add_transaction(
            initiator = uid, 
            transaction = {
                'sender': uid,
                'reciever': last_trans['reciever'],
                'value': package_details['value'],
                'type': 'RECIEVE_PACKAGE',
                'package_id': package_details['package_id']
            }
        )
        if type( res )== str :
            return make_response(jsonify({'message': res, 'code': 400}), 400)
        res = add_transaction(
            initiator = uid, 
            transaction = {
                'sender': "0",
                'reciever': last_trans['reciever'],
                'value': get_cost_to_ship(distance=find_path(source=last_trans['reciever'], destination=uid)['distance'], weight=package_details['weight']),
                'type': 'REWARD',
                'for_forwarding': package_details['package_id']
            }
        )
        package_details['destination'] = db.reference('/network/users/'+package_details['destination']).get()['place']
        return make_response(jsonify({'message': f"You just recieved this package from {db.reference('/network/users/'+last_trans['reciever']).get()['place']}", "package": package_details, 'code': 200}), 200)
    except Exception as e:
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/packages_to_be_delivered', methods = ['GET'])
def packages_to_be_delivered():
    try:
        uid = request.cookies.get("uid")
        if uid == None:
            return make_response(jsonify({'message': 'You are not a part of this network', 'code': 400}), 400)
        ref = db.reference('/blockchains/'+uid)
        blockchain = ref.get()
        blockchain = sorted(blockchain, key=lambda a: a['index'])
        packages = list(map( lambda trans: trans['package_id'], list(filter( lambda trans: 'type' in trans and (trans['type'] == 'REGISTER_PACKAGE' and trans['destination'] == uid), blockchain))))
        

        deliverable_packages_trans = list(map(lambda package: list(filter(lambda trans: 'package_id' in trans and trans['package_id'] == package, blockchain)), packages))
        deliverable_packages_trans = list(filter(lambda package_trans: package_trans[-1]['type'] == 'RECIEVE_PACKAGE' and package_trans[-1]['sender'] == uid, deliverable_packages_trans))
        packages = list(map(lambda package_trans: list(filter(lambda trans: trans['type'] == 'REGISTER_PACKAGE', package_trans))[0], deliverable_packages_trans))
        # packages = list(filter(lambda package: package['destination'] != uid, packages))
        # for package in packages:
        #     package['next_checkpoint'] = db.reference('/network/users/'+find_path(source=uid, destination=package['destination'])['path'][1]).get()['place']
        #     package['destination'] = db.reference('/network/users/'+package['destination']).get()['place']
        return make_response(jsonify({'message': {'packages':packages}, 'code': 200}), 200)
    except Exception as e:
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/deliver_package', methods=['POST'])
def deliver_package():
    try:
        uid = request.cookies.get("uid")
        if uid == None:
            return make_response(jsonify({'message': 'You are not a part of this network', 'code': 400}), 400)
        ref = db.reference('/blockchains/'+uid)
        transactions = list(ref.order_by_child('package_id').equal_to(request.json['package_id']).get().values())
        if len(transactions) == 0:
            return make_response(jsonify({'message': 'Package not found', 'code': 400}), 400)
        transactions = sorted(transactions, key=lambda a: a['index'])
        last_transaction = transactions[-1]
        if not (last_transaction['sender'] == uid and last_transaction['type'] == 'RECIEVE_PACKAGE'):
            return make_response(jsonify({'message': 'You cant Deliver this Package because you are not supposed to have this package', 'code': 400}), 400)
        
        package_details = list(filter(lambda a: a['type'] == 'REGISTER_PACKAGE', transactions))[0]
        if uid != package_details['destination']:
            return make_response(jsonify({'message': 'You cant deliver this package because you are not the destination of this package. You can only forward this package.', 'code': 400}), 400)
        
        if (package_details['pin'] != request.json['pin']):
            return make_response(jsonify({'message': 'Incorrect PIN', 'code': 400}), 400)


        res = add_transaction(
            initiator = uid, 
            transaction = {
                'sender': "0",
                'reciever': uid,
                'value': package_details['value'],
                'type': 'DELIVER_PACKAGE',
                'package_id': package_details['package_id']
            }
        )
        if type( res )== str :
            return make_response(jsonify({'message': res, 'code': 400}), 400)

        res = add_transaction(
            initiator = uid, 
            transaction = {
                'sender': "0",
                'reciever': uid,
                'value': get_cost_to_deliver(weight=package_details['weight']),
                'type': 'REWARD',
                'for_delivering': package_details['package_id']
            }
        )
        if type( res )== str :
            return make_response(jsonify({'message': res, 'code': 400}), 400)
        return make_response(jsonify({'message': f"Package with id {request.json['package_id']} is delivered to {package_details['owner_name']}", "package": package_details, 'code': 200}), 200)
    except Exception as e:
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)

@app.route('/track_package', methods=['POST'])
def track_package():
    try:
        uid = request.cookies.get("uid")
        checkpoints = db.reference('/network/users').get()
        initiator = copy.copy(uid)
        if initiator != None:
            initiator = random.choice(list(checkpoints.keys()))
            while checkpoints[initiator]['admin']:
                initiator = random.choice(list(checkpoints.keys()))
        transactions = list(db.reference('/blockchains/'+uid).order_by_child('package_id').equal_to(request.json['package_id']).get().values())
        if transactions==[]:
            return make_response(jsonify({'message': 'Package not Found.', 'code': 400}), 400)
        transactions = sorted(transactions, key=lambda a: a['index'])
        package_details = list(filter(lambda trans: trans['type'] == 'REGISTER_PACKAGE', transactions))[0]
        path = find_path(source=package_details['sender'], destination=package_details['destination'], detailed=True)
        track_details = path['track_details']
        distance = path['distance']
        path = path['path']
        extra_track_details = {}
        for trans in transactions:
            if trans['type'] == 'REGISTER_PACKAGE':
                message = 'Package registered at '+datetime.fromtimestamp(trans['timestamp']).strftime("%m/%d/%Y, %H:%M:%S")
                track_details[trans['sender']]['message'].append(message)
                message += (' at '+checkpoints[trans['sender']]['place'])
                distance_travelled = 0
            if trans['type'] == 'FORWARD_PACKAGE':
                message = 'Package forwarded at '+datetime.fromtimestamp(trans['timestamp']).strftime("%m/%d/%Y, %H:%M:%S")
                track_details[trans['reciever']]['message'].append(message)
                message += (' from '+checkpoints[trans['reciever']]['place'])
                distance_travelled = track_details[trans['reciever']]['distance_to_source'] + track_details[list(filter(lambda checkpoint: track_details[checkpoint]['previous_checkpoint'] == trans['reciever'], path))[-1]]['distance_to_previous_checkpoint'] / 2
                print(list(filter(lambda checkpoint: track_details[checkpoint]['previous_checkpoint'] == trans['reciever'], path))[-1])
            if trans['type'] == 'RECIEVE_PACKAGE':
                message = 'Package received at '+datetime.fromtimestamp(trans['timestamp']).strftime("%m/%d/%Y, %H:%M:%S")
                track_details[trans['sender']]['message'].append(message)
                message += (' at '+checkpoints[trans['sender']]['place'])
                distance_travelled = track_details[trans['sender']]['distance_to_source']
            if trans['type'] == 'DELIVER_PACKAGE':
                message = 'Package delivered at '+datetime.fromtimestamp(trans['timestamp']).strftime("%m/%d/%Y, %H:%M:%S")
                track_details[trans['reciever']]['message'].append(message)
                message += (' at '+checkpoints[trans['reciever']]['place'])
                distance_travelled = distance
        for checkpoint in track_details:
            track_details[checkpoint]['place'] = checkpoints[checkpoint]['place']
            if path.index(checkpoint) != 0:
                path.insert(path.index(checkpoint),'before_'+checkpoint)
                extra_track_details['before_'+checkpoint] = {
                    'distance': track_details[checkpoint]['distance_to_previous_checkpoint'],
                    'type': 'distance',
                    'position': math.floor((track_details[checkpoint]['distance_to_source']-track_details[checkpoint]['distance_to_previous_checkpoint']/2)*100/distance)
                }
        # print(track_details)
        track_details.update(extra_track_details)
        package_details['path'] = path
        package_details['track_details'] = track_details
        package_details['total_distance'] = distance
        package_details['distance_travelled'] = distance_travelled
        print(distance_travelled)
        return make_response(jsonify({'message': message, 'package_details':package_details, 'code': 200}), 200) 
    except Exception as e:
        return make_response(jsonify({'message': str(repr(e)), 'code': 400}), 400)    


@app.route('/admin')
def admin():
	return render_template('admin.html')

@app.route('/network')
def network():
	return render_template('network.html')

@app.route('/home')
def home():
	return render_template('home.html')

if __name__ == '__main__':
	app.debug = True
	app.run(host='127.0.0.1', port=5000)