import React, {useContext} from 'react';
import './App.css';
import { FirebaseContext } from './utils/firebase'
import 'firebase'
import {setCookie, getCookie} from './utils/cookie'
import Root from './Components/Root/Root'
import ResponsiveDrawer from "./Components/ResponsiveDrawer/ResponsiveDrawer"
import AdminDashboard from './Components/AdminDashboard/AdminDashboard'
import UserDashboard from '../src/Components/UserDashboard/UserDashboard'

import InboxIcon from '@material-ui/icons/MoveToInbox';

import AdminCheckpoints from "./Components/AdminCheckpoints/AdminCheckpoints"
import AdminNetwork from "./Components/AdminNetwork/AdminNetwork"

import RegisterPackage from "./Components/RegisterPackage/RegisterPackage"
import PackagesToBeForwarded from "./Components/PackagesToBeForwarded/PackagesToBeForwarded"
import ForwardedPackages from "./Components/ForwardedPackages/ForwardedPackages"
import ForwardPackage from "./Components/ForwardPackage/ForwardPackage"
import RecievePackage from "./Components/RecievePackage/RecievePackage"
import PackagesToBeDelivered from "./Components/PackagesToBeDelivered/PackagesToBeDelivered"
import DeliverPackage from "./Components/DeliverPackage/DeliverPackage"
import TrackPackage from "./Components/TrackPackage/TrackPackage"

import AddCircleIcon from '@material-ui/icons/AddCircle';
import CallReceivedIcon from '@material-ui/icons/CallReceived';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import RoomIcon from '@material-ui/icons/Room';
import SendIcon from '@material-ui/icons/Send';
import FlagIcon from '@material-ui/icons/Flag';
import SettingsInputComponentIcon from '@material-ui/icons/SettingsInputComponent';

const admin_routes = [
  {
      'text':'Checkpoints',
      'icon':<FlagIcon />,
      'component':AdminCheckpoints,
      // 'path': '/'
      'path':'/checkpoints'
  },
  {
      'text':'Network',
      'icon':<SettingsInputComponentIcon />,
      'component':AdminNetwork,
      'path':'/network'
  },
  {
    'text':'Track Package',
    'icon':<RoomIcon />,
    'component':TrackPackage,
    'path':'/trackpackage'
  },
]

const user_routes=[
  {
      'text':'Register new Package',
      'icon':<AddCircleIcon />,
      'component':RegisterPackage,
      // 'path': '/'
      'path':'/registerpackage'
  },
  {
    'text':'Recieve packages from previous checkpoint',
    'icon':<CallReceivedIcon />,
    'component':RecievePackage,
    'path':'/recievepackage'
  },
  {
      'text':'Packages to be Forwarded',
      'icon':<ArrowForwardIosIcon />,
      'component':PackagesToBeForwarded,
      'path':'/packagestobeforwarded'
  },
  {
      'text':'Forward Packages in Bulk',
      'icon':<ArrowForwardIosIcon />,
      'component': ForwardPackage,
      'path':'/forwardpackages'
  },
  {
      'text':'Packages Forwarded that are yet to recieve at next checkpoint',
      'icon':<HourglassEmptyIcon />,
      'component':ForwardedPackages,
      'path':'/forwardedpackages'
  },
  {
    'text':'Packages to be Delivered',
    'icon':<SendIcon />,
    'component':PackagesToBeDelivered,
    'path':'/packagestobedelivered'
  },
  {
    'text':'Deliver Packages in Bulk',
    'icon':<SendIcon />,
    'component':DeliverPackage,
    'path':'/deliverpackages'
  },
  {
    'text':'Track Package',
    'icon':<RoomIcon />,
    'component':TrackPackage,
    'path':'/trackpackage'
  },
  
]
function App() {
  const [isAdmin, setIsAdmin] = React.useState('');
  const [name, setName] = React.useState('');
  const firebase = useContext(FirebaseContext)
    if(getCookie('uid') !== "")
    {
      firebase.database().ref('/network/users/'+getCookie('uid')).once('value').then(function(snapshot){
        if(snapshot.val().admin)
        {
          setIsAdmin('true');
          setName(snapshot.val().name);
        }
        else
        {
          setIsAdmin('false');
          setName(snapshot.val().name);
        }
    }).catch(function(error){
      setIsAdmin('');
      setName('');
    })
    }
    
  return (
    <React.Fragment>
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"></link>
      {(getCookie('uid') === "" && isAdmin === '') && <Root setIsAdmin={setIsAdmin} setName={setName}/> }
      {(isAdmin === 'true') && <ResponsiveDrawer routes={admin_routes} setIsAdmin={setIsAdmin} setName={setName} name={name}/>}
      {(isAdmin === 'false') && <ResponsiveDrawer routes={user_routes} setIsAdmin={setIsAdmin} setName={setName} name={name}/>}
    </React.Fragment>
  );
}

export default App;
