import React, { createContext } from 'react'
import app from 'firebase/app'

const FirebaseContext = createContext(null)
export { FirebaseContext }

export default ({ children }) => {
    if (!app.apps.length) {
      app.initializeApp({
        apiKey: "AIzaSyAdyCX7KK9TK6wzGl8rrwFL4ZakdvbesfY",
        authDomain: "tracker-india.firebaseapp.com",
        databaseURL: "https://tracker-india.firebaseio.com",
        projectId: "tracker-india",
        storageBucket: "tracker-india.appspot.com",
        messagingSenderId: "1066790982214",
        appId: "1:1066790982214:web:68b9c2d60c39eebd8c7047",
      })
    }
    return (
      <FirebaseContext.Provider value={ app }>
        { children }
      </FirebaseContext.Provider>
    )
  }