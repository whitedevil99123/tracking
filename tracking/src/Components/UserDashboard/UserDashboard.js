import React, { Fragment } from 'react';

import { Router, Route} from 'react-router';
import { Link }  from 'react-router-dom';
import { createBrowserHistory } from "history";
import InboxIcon from '@material-ui/icons/MoveToInbox';


import ResponsiveDrawer from "./../ResponsiveDrawer/ResponsiveDrawer"

import RegisterPackage from "./../RegisterPackage/RegisterPackage"
import PackagesToBeForwarded from "./../PackagesToBeForwarded/PackagesToBeForwarded"
import ForwardedPackages from "./../ForwardedPackages/ForwardedPackages"
import ForwardPackage from "./../ForwardPackage/ForwardPackage"
import RecievePackage from "./../RecievePackage/RecievePackage"
import PackagesToBeDelivered from "./../PackagesToBeDelivered/PackagesToBeDelivered"
import DeliverPackage from "../DeliverPackage/DeliverPackage"
import TrackPackage from "../TrackPackage/TrackPackage"


const routes = [
    {
        'text':'Register new Package',
        'icon':<InboxIcon />,
        'component':RegisterPackage,
        'path':'/registerpackage'
    },
    {
      'text':'Recieve packages from previous checkpoint',
      'icon':<InboxIcon />,
      'component':RecievePackage,
      'path':'/recievepackage'
    },
    {
        'text':'Packages to be Forwarded',
        'icon':<InboxIcon />,
        'component':PackagesToBeForwarded,
        'path':'/packagestobeforwarded'
    },
    {
        'text':'Forward Packages in Bulk',
        'icon':<InboxIcon />,
        'component': ForwardPackage,
        'path':'/forwardpackages'
    },
    {
        'text':'Packages Forwarded that are yet to recieve at next checkpoint',
        'icon':<InboxIcon />,
        'component':ForwardedPackages,
        'path':'/forwardedpackages'
    },
    {
      'text':'Packages to be Delivered',
      'icon':<InboxIcon />,
      'component':PackagesToBeDelivered,
      'path':'/packagestobedelivered'
    },
    {
      'text':'Deliver Packages in Bulk',
      'icon':<InboxIcon />,
      'component':DeliverPackage,
      'path':'/deliverpackages'
    },
    {
      'text':'Track Package',
      'icon':<InboxIcon />,
      'component':TrackPackage,
      'path':'/trackpackage'
    },
]
const history = createBrowserHistory();
const UserDashboard = (props) => {

      return(
        <Router history={history}>  
            <ResponsiveDrawer/>
            

        </Router>
      );
    
  }

  export default UserDashboard;

