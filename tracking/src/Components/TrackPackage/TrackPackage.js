import React, {Fragment} from "react";
import "react-step-progress-bar/styles.css";
import { ProgressBar, Step } from "react-step-progress-bar";
import FontAwesome from 'react-fontawesome'

import Loading from './../Loading/Loading'

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { withStyles, makeStyles } from '@material-ui/core/styles';

import PackageCard from './../PackageCard/PackageCard'


// import faStyles from 'font-awesome/css/font-awesome.css'
import './TrackPackage.css'



const HtmlTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: '#f5f5f9',
      color: 'rgba(0, 0, 0, 0.87)',
      maxWidth: 'none',
      fontSize: theme.typography.pxToRem(12),
      border: '1px solid #dadde9',
    },
  }))(Tooltip);
const TrackPackage = (props) => {

    const [packageID, setPackageID] = React.useState('');
    const [packageDetails, setPackageDetails] = React.useState('');
    const [message, setMessage] = React.useState('');
    const [loading, setLoading] = React.useState(false);

    return(
        <Fragment>
            <script src="http://code.jquery.com/jquery-latest.min.js"></script>
            <script type="text/javascript" src="jquery-barcode.js"></script>
            <FormControl>
                <InputLabel >Package ID</InputLabel>
                <Input autoFocus={true} value={packageID} onChange={(event) => {setPackageID(event.target.value)}} onKeyDown={(event) => {
                    if (event.keyCode === 13) {
                        setLoading(true);
                        fetch('/track_package',{
                            method: 'POST',
                            headers: {
                                'Content-type': 'application/json',
                                'Accept': 'application/json'
                            },
                            body: JSON.stringify({'package_id':packageID})
                        }).then(res => res.json()).then(data=>{
                            if(data.code === 400)
                            {
                                setMessage(data.message);
                                setPackageDetails('')
                                setPackageID('')
                                setLoading(false);
                            }
                            else
                            {
                                setPackageDetails(data.package_details);
                                setMessage(data.message)
                                setPackageID('')
                                setLoading(false);
                            }    
                        }).catch(error=>{
                            console.log(error);
                            setLoading(false);
                        })
                    }
                }}/>
            </FormControl>
            

            
            {message != '' && packageDetails != '' &&
                    <ProgressBar
                    percent={packageDetails['distance_travelled']*100/packageDetails['total_distance']}
                    // filledBackground="linear-gradient(to right, #fefb72, #f0bb31)"
                    stepPositions={packageDetails['path'].map((step)=>packageDetails['track_details'][step]['position'])}
                    className="progress-bar"
                    >
                        {packageDetails['path'].map(function(step){
                            console.log(packageDetails['track_details']);
                            if (packageDetails['track_details'][step]['type'] == 'checkpoint')
                            return(
                            <Step position={1}>
                            {({ accomplished }) => (
                                <HtmlTooltip arrow
                                    title={
                                        <React.Fragment>
                                            {packageDetails['track_details'][step]['message'].map(toolTiPString => 
                                                <Typography>{toolTiPString}</Typography>
                                            )}
                                        </React.Fragment>
                                    }
                                >
                                <Box display="flex" flexDirection="column" justifyContent="center" m={1} p={1} style={{}}>
                                    <Box p={1}>
                                        <FontAwesome
                                            className="super-crazy-colors"
                                            name="flag"
                                            // cssModule={faStyles}
                                            size="3x"
                                            style={{ marginLeft:'37px', textShadow: '0 0 3px rgba(0, 0, 0, 1)', color:"yellow", filter: `grayscale(${accomplished ? 0 : 100}%)` }}
                                        />
                                    </Box>
                                    <Box p={1}>
                                        <div style={{fontWeight:'bold', fontSize:'20px', textAlign:'center'}}>{packageDetails['track_details'][step]['place']}</div>
                                    </Box>
                                   
                                </Box>
                              </HtmlTooltip>
                                
                            )}
                            </Step>
                            )
                            if (packageDetails['track_details'][step]['type'] == 'distance')
                            return(
                                <Step position={1}>
                                {({ accomplished }) => (
                                    <div style={{color:'red', fontSize:'20px', fontWeight:'bold', marginTop:'30px'}}>
                                        {packageDetails['track_details'][step]['distance']} Kms
                                    </div>
                                )}
                                </Step>
                                )
                        }
                        )}
                    </ProgressBar>
            }
            {message != '' &&
                <PackageCard message={message} packageDetails={packageDetails===''?'':{
                    'Package ID': packageDetails['package_id'],
                    'Weight': packageDetails['weight']+' Kgs',
                    'Value': packageDetails['value'],
                    'Owner Name': packageDetails['owner_name'],
                    'Ph. No.': packageDetails['mob'],
                    'Distance Travelled': packageDetails['distance_travelled']+"kms/"+packageDetails['total_distance']+'kms',
                    'Remaining Distance': (packageDetails['total_distance']-packageDetails['distance_travelled'])+"kms/"+packageDetails['total_distance']+'kms'
                }}>

                </PackageCard>
            }
        { loading && <Loading />}
        </Fragment>
        
    )
}

export default TrackPackage;