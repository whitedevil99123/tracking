import React, { Fragment, useContext, useRef } from 'react';

// import Barcode from "react-native-barcode-builder";
import PackageCard from './../PackageCard/PackageCard'
import Loading from './../Loading/Loading'

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import LinkIcon from '@material-ui/icons/Link';

import { FirebaseContext } from '../../utils/firebase'
import 'firebase'

import {setCookie, getCookie} from '../../utils/cookie'

const useStyles = makeStyles({

    card: {
      minWidth: 275,
      maxWidth: 500
    }
  });

const DeliverPackage = (props) => {
    const firebase = useContext(FirebaseContext)
    const classes = useStyles();
    const [packageID, setPackageID] = React.useState('');
    const [pin, setPin] = React.useState('');
    const [packageDetails, setPackageDetails] = React.useState('');
    const [message, setMessage] = React.useState('');
    const [loading, setLoading] = React.useState(false);
    const packageIDref = useRef();

    return(
        <Fragment>
            <FormControl>
                <InputLabel >Package ID</InputLabel>
                <Input ref={packageIDref} autoFocus={true} value={packageID} onChange={(event) => {setPackageID(event.target.value)}} />
            </FormControl>
            <FormControl>
                <InputLabel >PIN</InputLabel>
                <Input value={pin} onChange={(event) => {setPin(event.target.value)}} type="password" onKeyDown={(event) => {
                    if (event.keyCode === 13) {
                        setLoading(true);
                        fetch('/deliver_package',{
                            method: 'POST',
                            headers: {
                                'Content-type': 'application/json',
                                'Accept': 'application/json'
                            },
                            body: JSON.stringify({'package_id':packageID, 'pin':pin})
                        }).then(res => res.json()).then(data=>{
                            if(data.code === 400)
                            {
                                setMessage(data.message);
                                setPackageDetails('')
                                setPackageID('')
                                setPin('');
                                packageIDref.current.children[0].focus();
                                packageIDref.focus();
                                setLoading(false);
                            }
                            else
                            {
                                setPackageDetails(data.package);
                                setMessage(data.message)
                                setPackageID('');
                                setPin('');
                                packageIDref.current.children[0].focus();
                                setLoading(false);
                            }    
                        }).catch(error=>{
                            console.log(error);
                            setLoading(false);
                        })
                    }
                }}/>
            </FormControl>
            { message !== '' && 
        <PackageCard message={message} packageDetails={packageDetails===''?'':{
            'Package ID': packageDetails['package_id'],
            'Weight': packageDetails['weight']+' Kgs',
            'Value': packageDetails['value'],
            'Owner Name': packageDetails['owner_name'],
            'Ph. No.': packageDetails['mob']
        }}>
        </PackageCard>
        }
        
        { loading && <Loading />}
      </Fragment>
    );
  
}

export default DeliverPackage;