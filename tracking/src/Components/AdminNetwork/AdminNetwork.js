import React, { Fragment, useEffect, useContext } from 'react';
import Graph from "react-graph-vis";
import { FirebaseContext } from '../../utils/firebase'
import 'firebase'

import Loading from './../Loading/Loading'

import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import LinkIcon from '@material-ui/icons/Link';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

const AdminNetwork = (props) => {
    const firebase = useContext(FirebaseContext)
    const [nodes, setNodes] = React.useState({});
    const [edges, setEdges] = React.useState({});
    const [selectedEdge, setSelectedEdge] = React.useState(undefined);
    const [selectedNode1, setSelectedNode1] = React.useState('');
    const [selectedNode2, setSelectedNode2] = React.useState('');
    const [distance, setDistance] = React.useState(0);
    const [loading, setLoading] = React.useState(false);
    useEffect(() => {
        var ref = firebase.database().ref('/network/users/')
        ref.on("child_added", function (snapshot) {
            setNodes((prevState) => {
                var newState = { ...prevState };
                newState[snapshot.key] = snapshot.val()
                return newState;
            })
        })
        ref.on("child_changed", function (snapshot) {
            setNodes((prevState) => {
                var newState = { ...prevState };
                newState[snapshot.key] = snapshot.val()
                return newState;
            })
        })
        ref.on("child_removed", function (snapshot) {
            setNodes((prevState) => {
                var newState = { ...prevState };
                delete newState[snapshot.key]
                return newState;
            })
        })
        var ref = firebase.database().ref('/network/connections/')
        ref.on("child_added", function (snapshot) {
            setEdges((prevState) => {
                var newState = { ...prevState };
                newState[snapshot.key] = snapshot.val();
                return newState;
            })
        })
        ref.on("child_changed", function (snapshot) {
            setEdges((prevState) => {
                var newState = { ...prevState };
                newState[snapshot.key] = snapshot.val()
                return newState;
            })
        })
        ref.on("child_removed", function (snapshot) {
            setEdges((prevState) => {
                var newState = { ...prevState };
                delete newState[snapshot.key];
                return newState;
            })
        })
    }, [])


    const options = {
        physics: {
            enabled: false
        },
        layout: {
            hierarchical: false
        },
        edges: {
            color: "#000000"
        }
    };


    var networkRef = React.createRef();
    const events = {
        dragEnd: function (event) {
            var data = networkRef.current.Network.getPositions();
            var newData = {};
            for (let key in data){
                newData[key+'/x'] = data[key]['x'];
                newData[key+'/y'] = data[key]['y'];
            }
            firebase.database().ref('/network/users/').update(newData);
        },
        selectEdge: function (event) {
            if(event.edges.length==1)
                setSelectedEdge(event.edges[0]);
            else
                setSelectedEdge(undefined);
        },
        deselectEdge: function (event) {
            if(event.edges.length==1)
                setSelectedEdge(event.edges[0]);
            else
                setSelectedEdge(undefined);
        }

    };
    const classes = useStyles();
    return (
        <Fragment>
            <Box display="flex" flexDirection="column" p={1} m={1} bgcolor="background.paper">
            <Box>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">CheckPoint1</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selectedNode1}
                    onChange={(event)=>{setSelectedNode1(event.target.value)}}
                >
                    {Object.entries(nodes).filter(function(pair){
                        return !pair[1].admin
                    }).map(function(pair){
                        return(
                            <MenuItem value={pair[0]}>{pair[1].name+'('+pair[1].place+')'}</MenuItem>
                        )
                    })}
                </Select>
            </FormControl>
            
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">CheckPoint2</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selectedNode2}
                    onChange={(event)=>{setSelectedNode2(event.target.value)}}
                >
                    {Object.entries(nodes).filter(function(pair){
                        return !pair[1].admin
                    }).map(function(pair){
                        return(
                            <MenuItem value={pair[0]}>{pair[1].name+'('+pair[1].place+')'}</MenuItem>
                        )
                    })}
                </Select>
            </FormControl>
            <FormControl>
                <InputLabel htmlFor="component-simple">Distance(in Kms)</InputLabel>
                <Input id="component-simple" value={distance} onChange={(event)=>{setDistance(parseInt(event.target.value))}} type='number'/>
            </FormControl>
            <Button
                variant="contained"
                color="primary"
                className={classes.button}
                startIcon={<LinkIcon />}
                disabled={ selectedNode1 === '' || selectedNode2 === '' || distance === 0 }
                onClick={()=>{
                    setLoading(true);
                    fetch('/connections',{
                        method: 'POST',
                        headers: {
                            'Content-type': 'application/json',
                            'Accept': 'application/json'
                        },
                        body: JSON.stringify({node1:selectedNode1,node2:selectedNode2,distance:distance})
                    }).then(res => res.json()).then(data=>{
                        alert(data.message);
                        setLoading(false);
                    }).catch(error=>{
                        console.log(error);
                        setLoading(false);
                    })
                }}
            >
                Connect Nodes
            </Button>
            </Box>
            <Box>
            <Button
                variant="contained"
                color="secondary"
                // className={classes.button}
                startIcon={<DeleteIcon />}
                disabled={!selectedEdge}
                onClick={()=>{
                    setLoading(true);
                    fetch('/connections',{
                        method: 'DELETE',
                        headers: {
                            'Content-type': 'application/json',
                            'Accept': 'application/json'
                        },
                        body: JSON.stringify({edge:selectedEdge})
                    }).then(res => res.json()).then(data=>{
                        alert(data.message);
                        setLoading(false);
                    }).catch(error=>{
                        console.log(error);
                        setLoading(false);
                    })
                }}
            >
                Delete Selected Connection
            </Button>
            </Box>
            <Graph
                graph={{
                    nodes: Object.entries(nodes).map(function (pair) {
                        return {
                            id: pair[0],
                            label: pair[1].name+'('+pair[1].place+')',
                            shape: 'icon',
                            icon: { face: 'FontAwesome', code: pair[1].admin ? '\uf007' : '\uf024' },
                            x: pair[1].x,
                            y: pair[1].y
                        }
                    }),
                    edges: Object.entries(edges).map(function (pair) {
                        return {
                            id: pair[0],
                            from: pair[1]['from'],
                            to: pair[1]['to'],
                            arrows: {
                                to: {
                                    enabled: false,
                                }
                            },
                            label: pair[1]['distance']+' kms'
                        }
                    })
                }}
                options={options} events={events} style={{ height: "640px" }} ref={networkRef}
                />
                </Box>
                { loading && <Loading />}
            </Fragment>
    );

}

export default AdminNetwork;