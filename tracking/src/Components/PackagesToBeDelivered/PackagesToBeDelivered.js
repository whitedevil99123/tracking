import React, { Fragment, useEffect, useState, forwardRef } from 'react';

import MaterialTable from 'material-table';

import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';

import Loading from './../Loading/Loading'

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import ForwardIcon from '@material-ui/icons/Forward';
import RefreshIcon from '@material-ui/icons/Refresh';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

const PackagesToBeDelivered = (props) => {
    const [packages, setPackages] = useState([]);
    const [loading, setLoading] = React.useState(false);

    const DeliverPackage = (props) => {
        const [pin, setPin] = React.useState('');
    
        return(
            <Fragment>
                <FormControl>
                    <InputLabel htmlFor="component-simple">Pin</InputLabel>
                    <Input id="component-simple" value={pin} onChange={(event)=>{setPin(event.target.value)}} type='password'/>
                </FormControl>
                <br/>
                <IconButton disabled={isNaN(pin) || pin.length !== 4} onClick={(event)=>{
                    setLoading(true);
                    fetch('/deliver_package',{
                        method: 'POST',
                        headers: {
                            'Content-type': 'application/json',
                            'Accept': 'application/json'
                        },
                        body: JSON.stringify({'package_id':props.rowData.package_id, 'pin':pin})
                    }).then(res => res.json()).then(data=>{
                        alert(data.message);
                        fetchPackages();
                        setLoading(false);
                    }).catch(error=>{
                        console.log(error);
                        setLoading(false);
                    })
                }}>
                    <ForwardIcon />
                </IconButton>
            </Fragment>
        )
    }

    const fetchPackages = () => {
        setLoading(true);
        fetch('/packages_to_be_delivered',{
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/json'
            }
        }).then(res => res.json()).then(data=>{
            if(data.code === 400)
            {    alert(data.message)
                setLoading(false);
            }
            else
            {    setPackages(data.message.packages)
                setLoading(false);
            }
        }).catch(error=>{
            console.log(error);
            setLoading(false);
        })
    }
    useEffect(() => {
        fetchPackages();
    }, [])
    return(
        <Fragment>
        <MaterialTable
        icons={tableIcons}
        title="Packages to be Delivered"
        columns={[
            { 'title': 'Deliver Package', 'field': 'package_id', 'editable':'never', 
                'render': rowData => <DeliverPackage rowData={rowData}></DeliverPackage>
            },
            { 'title': 'Package ID', 'field': 'package_id' },
            { 'title': 'Value', 'field': 'value' },
            { 'title': 'Owner Name', 'field': 'owner_name' },
            { 'title': 'Ph. No.', 'field': 'mob' }
        ]}
        data={packages}
        actions={[
            // {
            //     'icon': ForwardIcon,
            //     'tooltip': 'Forward Package',
            //     'onClick': (event, rowData) => {
            //         fetch('/forward_package',{
            //             method: 'POST',
            //             headers: {
            //                 'Content-type': 'application/json',
            //                 'Accept': 'application/json'
            //             },
            //             body: JSON.stringify({'package_id':rowData.package_id})
            //         }).then(res => res.json()).then(data=>{
            //             fetchPackages();
            //             alert(data.message);
            //         }).catch(error=>{
            //             console.log(error);
            //         })
            //     }
                
            // },
            {
                icon: RefreshIcon,
                tooltip: 'Refresh Data',
                isFreeAction: true,
                onClick: (event) => {fetchPackages()}
            }
        ]}
    />
    { loading && <Loading />}
    </Fragment>
    );
  
}



export default PackagesToBeDelivered;