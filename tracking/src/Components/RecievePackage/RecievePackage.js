import React, { Fragment, useContext } from 'react';

// import Barcode from "react-native-barcode-builder";
import PackageCard from './../PackageCard/PackageCard'

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import LinkIcon from '@material-ui/icons/Link';

import Loading from './../Loading/Loading'

import { FirebaseContext } from '../../utils/firebase'
import 'firebase'

import {setCookie, getCookie} from '../../utils/cookie'

const useStyles = makeStyles({

    card: {
      minWidth: 275,
      maxWidth: 500
    }
  });

const RecievePackage = (props) => {
    const firebase = useContext(FirebaseContext)
    const classes = useStyles();
    const [packageID, setPackageID] = React.useState('');
    const [packageDetails, setPackageDetails] = React.useState('');
    const [message, setMessage] = React.useState('');
    const [loading, setLoading] = React.useState(false);

    return(
        <Fragment>
            <FormControl>
                <InputLabel >Package ID</InputLabel>
                <Input autoFocus={true} value={packageID} onChange={(event) => {setPackageID(event.target.value)}} onKeyDown={(event) => {
                    if (event.keyCode === 13) {
                        setLoading(true);
                        fetch('/recieve_package',{
                            method: 'POST',
                            headers: {
                                'Content-type': 'application/json',
                                'Accept': 'application/json'
                            },
                            body: JSON.stringify({'package_id':packageID})
                        }).then(res => res.json()).then(data=>{
                            if(data.code === 400)
                            {
                                setMessage(data.message);
                                setPackageDetails('')
                                setPackageID('')
                                setLoading(false)
                            }
                            else
                            {
                                setPackageDetails(data.package);
                                setMessage(data.message)
                                setPackageID('')
                                setLoading(false)
                            }    
                        }).catch(error=>{
                            console.log(error);
                            setLoading(false)
                        })
                    }
                }}/>
            </FormControl>
        { message !== '' && 
        <PackageCard message={message} packageDetails={packageDetails===''?'':{
            'Package ID': packageDetails['package_id'],
            'Weight': packageDetails['weight']+' Kgs',
            'Value': packageDetails['value'],
            'Owner Name': packageDetails['owner_name'],
            'Ph. No.': packageDetails['mob']
        }}>
        </PackageCard>
        }
            

            { loading && <Loading />}
      </Fragment>
    );
  
}

export default RecievePackage;