import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import $ from 'jquery'
import barcode from './../jquery-barcode.js'

const useStyles = makeStyles((theme) => ({
  
  right: {
    padding: theme.spacing(2),
    textAlign: 'right',
    color: theme.palette.text.secondary,
    fontSize: '30px',
  },
  left: {
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.secondary,
    fontSize: '30px',
  },
  center: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: 'green',
    fontSize: '30px',
    margin: 'auto'
  },
  error: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: 'red',
    fontSize: '30px',
  }
}));

export default function PackageCard(props) {
  const classes = useStyles();
  var btype = 'code128';
    var settings = {
        output:'bmp',
        bgColor: '#FFFFFF',
        color: '#000000',
        barWidth: '1',
        barHeight: '50',
        showHRI: true
    };
    barcode($("#barcode-target"),props.packageDetails['Package ID'], btype, settings);
  return (
    <Card className={classes.card} variant="outlined">
      <CardContent>
        <Grid container spacing={1}>
          <Grid item xs={12}>
          <div className={props.packageDetails == '' ? classes.error : classes.center} style={'fontSize' in props ? {fontSize:props.fontSize} : {}}>{props.message}</div>
          </Grid>
          { props.packageDetails != '' && 
            <Grid item xs={12}>
              <div id="barcode-target" className={classes.center}></div>
            </Grid>
          }
          { props.packageDetails != '' && 
            Object.entries(props.packageDetails).map(([key, value]) =>
            ( 
              <Fragment>
                <Grid item xs={6}>
                  <div className={classes.right}>{key} :</div>
                </Grid>
                <Grid item xs={6}>
                  <div className={classes.left}>{value}</div>
                </Grid>
              </Fragment>  
            ))

          }
          
        </Grid>
      </CardContent>
    </Card>
  );
}
