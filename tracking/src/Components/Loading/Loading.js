import React, { Fragment, useContext } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import './styles.scss'

const Loading = (props) => {
    return(
        <div className="loading" >

            <div className="modal-content">
        <CircularProgress size={50} />
        </div>    </div>
    )
}

export default Loading;