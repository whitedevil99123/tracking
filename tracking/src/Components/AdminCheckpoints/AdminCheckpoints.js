import React, { forwardRef, useContext, useEffect, Fragment } from 'react';
import MaterialTable from 'material-table';

import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';

import Loading from './../Loading/Loading'

import { FirebaseContext } from '../../utils/firebase'
import 'firebase'

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { Button } from '@material-ui/core';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};


const AdminCheckpoints = (props) => {
    const firebase = useContext(FirebaseContext)
    const [data, setData] = React.useState({});
    const [loading, setLoading] = React.useState(false);
    useEffect(() => {
        var ref = firebase.database().ref('/network/users/')
        ref.on("child_added", function (snapshot) {
            setData((prevState) => {
                var newState = { ...prevState };
                newState[snapshot.key] = snapshot.val()
                return newState;
            })
        })
        ref.on("child_changed", function(snapshot) {
            setData((prevState) => {
                var newState = { ...prevState };
                newState[snapshot.key] = snapshot.val()
                return newState;
            })
        })
        ref.on("child_removed", function(snapshot) {
            setData((prevState) => {
                var newState = { ...prevState };
                delete newState[snapshot.key]
                return newState;
            })
        })
    }, [])


    return (
        <Fragment>
        <MaterialTable
            icons={tableIcons}
            title="Editable Example"
            columns={[
                { 'title': 'Manage Coins', 'field': 'uid', 'editable':'never', 
                    'render': rowData => <AddorRemoveCoins rowData={rowData}></AddorRemoveCoins>
                },
                { 'title': 'Checkpoint ID', 'field': 'uid', 'editable': 'none' },
                { 'title': 'Name', 'field': 'name' },
                { 'title': 'Place', 'field': 'place' },
                { 'title': 'Email', 'field': 'email', 'editable': 'onAdd' },
                { 'title': 'Phone. No.', 'field': 'phno' },
            ]}
            actions={[
                {
                    'icon': AttachMoneyIcon,
                    'tooltip': 'Check Coins',
                    'onClick': (event, rowData) => {
                        setLoading(true);
                        fetch('/check_coins/'+rowData.uid,{
                            method: 'GET',
                            headers: {
                                'Content-type': 'application/json',
                                'Accept': 'application/json'
                            }
                        }).then(res => res.json()).then(data=>{
                            alert(data.message);
                            setLoading(false);
                        }).catch(error=>{
                            console.log(error);
                            setLoading(false);
                        })
                    }
                    
                }
            ]}
            data={Object.entries(data).filter(function(pair){
                return !pair[1].admin
            }).map(function(pair) {
                return { ...pair[1], uid:pair[0] }
            })}
            editable={{
                onRowAdd: (newData) =>
                    new Promise((resolve) => {
                        setTimeout(() => {
                            resolve();
                            fetch('/checkpoints',{
                                method: 'POST',
                                headers: {
                                    'Content-type': 'application/json',
                                    'Accept': 'application/json'
                                },
                                body: JSON.stringify(newData)
                            }).then(res => res.json()).then(data=>{
                                alert(data.message);
                            }).catch(error=>{
                                console.log(error);
                            })

                        }, 600);
                    }),
                onRowUpdate: (newData, oldData) =>
                    new Promise((resolve) => {
                        setTimeout(() => {
                            resolve();
                            if (oldData) {
                                
                                fetch('/checkpoints',{
                                    method: 'PUT',
                                    headers: {
                                        'Content-type': 'application/json',
                                        'Accept': 'application/json'
                                    },
                                    body: JSON.stringify(newData)
                                }).then(res => res.json()).then(data=>{
                                    alert(data.message);
                                }).catch(error=>{
                                    console.log(error);
                                })
                            }
                        }, 600);
                    }),
                onRowDelete: (oldData) =>
                    new Promise((resolve) => {
                        setTimeout(() => {
                            resolve();
                            fetch('/checkpoints',{
                                method: 'DELETE',
                                headers: {
                                    'Content-type': 'application/json',
                                    'Accept': 'application/json'
                                },
                                body: JSON.stringify(oldData)
                            }).then(res => res.json()).then(data=>{
                                alert(data.message);
                            }).catch(error=>{
                                console.log(error);
                            })
                        }, 600);
                    }),
            }}
        />
        { loading && <Loading />}
        </Fragment>
    );
}

const AddorRemoveCoins = (props) => {
    const [coins, setCoins] = React.useState();
    const [loading, setLoading] = React.useState(false);
    return(
        <Fragment>
            <FormControl>
                <InputLabel htmlFor="component-simple">Coins</InputLabel>
                <Input id="component-simple" value={coins} onChange={(event)=>{setCoins(parseInt(event.target.value))}} type='number'/>
            </FormControl>
            <br/>
            <IconButton disabled={isNaN(coins)} onClick={(event)=>{
                setLoading(true);
                fetch('/add_coins',{
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                        'Accept': 'application/json'
                    },
                    body: JSON.stringify({'reciever':props.rowData.uid, 'value':coins})
                }).then(res => res.json()).then(data=>{
                    alert(data.message);
                    setLoading(false);
                }).catch(error=>{
                    console.log(error);
                    setLoading(false);
                })
            }}>
                <AddIcon />
            </IconButton>
            <IconButton disabled={isNaN(coins)} onClick={(event)=>{
                setLoading(true);
                fetch('/remove_coins',{
                    method: 'POST',
                    headers: {
                        'Content-type': 'application/json',
                        'Accept': 'application/json'
                    },
                    body: JSON.stringify({'sender':props.rowData.uid, 'value':coins})
                }).then(res => res.json()).then(data=>{
                    alert(data.message);
                    setLoading(false);
                }).catch(error=>{
                    console.log(error);
                    setLoading(false);
                })
            }}>
                <RemoveIcon />
            </IconButton>
            { loading && <Loading />}
        </Fragment>
    )
}

export default AdminCheckpoints;