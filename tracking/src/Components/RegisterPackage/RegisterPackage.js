import React, { Fragment, useContext, useEffect } from 'react';

// import Barcode from "react-native-barcode-builder";

import PackageCard from './../PackageCard/PackageCard'

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import LinkIcon from '@material-ui/icons/Link';

import Loading from './../Loading/Loading'


import { FirebaseContext } from '../../utils/firebase'
import 'firebase'

import {setCookie, getCookie} from '../../utils/cookie'

const useStyles = makeStyles({

    card: {
      minWidth: 275,
      maxWidth: 500
    }
  });

const RegisterPackage = (props) => {
    const firebase = useContext(FirebaseContext)
    const classes = useStyles();
    const [name, setName] = React.useState('');
    const [phNo, setPhNo] = React.useState('');
    const [value, setValue] = React.useState();
    const [weight, setWeight] = React.useState();
    const [destination, setDestination] = React.useState('');
    const [nodes, setNodes] = React.useState({});
    const [packageDetails, setPackageDetails] = React.useState('');
    const [message, setMessage] = React.useState('');
    const [loading, setLoading] = React.useState(false);
    useEffect(() => {
        var ref = firebase.database().ref('/network/users/')
        ref.on("child_added", function (snapshot) {
            setNodes((prevState) => {
                var newState = { ...prevState };
                newState[snapshot.key] = snapshot.val()
                return newState;
            })
        })
    }, [])

    return(
        <Fragment>
        { message === '' && <Card className={classes.card} variant="outlined">
        <CardContent>
        <Box flexDirection="column" justifyContent="middle">
        <Box alignSelf="middle">
            <FormControl>
                <InputLabel >Package Owner Name</InputLabel>
                <Input value={name} onChange={(event) => {setName(event.target.value)}} />
            </FormControl>
        </Box>
        <Box alignSelf="center">
            <FormControl>
                <InputLabel >Phone No.</InputLabel>
                <Input value={phNo} onChange={(event) => {setPhNo(event.target.value)}} />
            </FormControl>
        </Box>
        <Box alignSelf="center">
            <FormControl>
                <InputLabel >Package Value</InputLabel>
                <Input value={value} onChange={(event) => {setValue(parseInt(event.target.value))}} type="number"/>
            </FormControl>
        </Box>
        <Box alignSelf="center">
            <FormControl>
                <InputLabel >Package Weight(in Kgs)</InputLabel>
                <Input value={weight} onChange={(event) => {setWeight(parseInt(event.target.value))}} type="number"/>
            </FormControl>
        </Box>
        <Box alignSelf="center">
            <FormControl className={classes.formControl}>
                <InputLabel>Destination</InputLabel>
                <Select
                    value={destination}
                    onChange={(event)=>{setDestination(event.target.value)}}
                >
                    {Object.entries(nodes).filter(function(pair){
                        return (!pair[1].admin && pair[0] !== getCookie('uid'))
                    }).map(function(pair){
                        return(
                            <MenuItem value={pair[0]}>{pair[1].place}</MenuItem>
                        )
                    })}
                </Select>
            </FormControl>
        </Box>
        <Box alignSelf="center">
        <Button
                variant="contained"
                color="primary"
                className={classes.button}
                startIcon={<LinkIcon />}
                disabled={ name === '' || phNo === '' || value === 0 || destination === '' }
                onClick={()=>{
                    setLoading(true);
                    fetch('/register_package',{
                        method: 'POST',
                        headers: {
                            'Content-type': 'application/json',
                            'Accept': 'application/json'
                        },
                        body: JSON.stringify({owner_name:name,mob:phNo,destination:destination,value:value,weight:weight})
                    }).then(res => res.json()).then(data=>{
                        if(data.code === 400)
                        {
                            setMessage(data.message);
                            setPackageDetails('');
                            setLoading(false);
                        }
                        else
                        {
                            setPackageDetails(data.package_details);
                            setMessage(data.message);
                            setLoading(false)
                        }
                    }).catch(error=>{
                        console.log(error);
                        setLoading(false)
                    })
                }}
            >
                Register Package
            </Button>
            </Box>
        </Box>
        </CardContent>
      </Card>}
      
      { message !== '' && 
        <Fragment>
            <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    startIcon={<LinkIcon />}
                    onClick={()=>{
                        setPackageDetails('');
                        setMessage('');
                        setName('');
                        setPhNo('');
                        setValue();
                        setWeight();
                        setDestination('');
                    }}
            >
                Register New Package
            </Button>
            <PackageCard message={message} packageDetails={packageDetails===''?'':{
                'Package ID': packageDetails['package_id'],
                'Weight': packageDetails['weight']+' Kgs',
                'Value': packageDetails['value'],
                'Owner Name': packageDetails['owner_name'],
                'Ph. No.': packageDetails['mob'],
                'Source': packageDetails['path'][0],
                'Destination': packageDetails['path'][packageDetails['path'].length-1],
                'Expected Path': (packageDetails['path'].filter(function(place, index){return index!= packageDetails['path'].length-1 }).map(function(place){return(place+'->')}) + packageDetails['path'][packageDetails['path'].length-1]),
                'Cost to Customer': packageDetails['cost_to_customer']
            }}>
            </PackageCard>
        </Fragment>
        }
        { loading && <Loading />}
      </Fragment>
    );
  
}

export default RegisterPackage;