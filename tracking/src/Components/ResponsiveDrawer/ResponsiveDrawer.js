import React, { Fragment } from 'react';

import { Router, Route, Switch} from 'react-router';
import { Link }  from 'react-router-dom';
import { createBrowserHistory } from "history";
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import {setCookie, getCookie} from '../../utils/cookie'

import RegisterPackage from "./../RegisterPackage/RegisterPackage"
import PackagesToBeForwarded from "./../PackagesToBeForwarded/PackagesToBeForwarded"
import ForwardedPackages from "./../ForwardedPackages/ForwardedPackages"
import ForwardPackage from "./../ForwardPackage/ForwardPackage"
import RecievePackage from "./../RecievePackage/RecievePackage"
import PackagesToBeDelivered from "./../PackagesToBeDelivered/PackagesToBeDelivered"
import DeliverPackage from "../DeliverPackage/DeliverPackage"
import TrackPackage from "../TrackPackage/TrackPackage"
import { Box } from '@material-ui/core';

import './styles.css'
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const history = createBrowserHistory();




  
  function ResponsiveDrawer(props) {
    console.log(props)
    const { window } = props;
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);
  
    const handleDrawerToggle = () => {
      setMobileOpen(!mobileOpen);
    };
    
    

    function DrawerList(props1) {
        console.log(props1)
    return(
      <div>
        <div className={classes.toolbar} />
        <List>
        <Divider />
          {props1.routes.map((route) => (
              <Fragment>
                <Link to={route['path']} key={route['text']} style={{textDecoration:'none'}}>
                    <ListItem button style={{backgroundColor:`${props1.location['pathname']==route['path'] && "rgb(63,81,181)"}`, color:`${props1.location['pathname']==route['path'] ? "white" : "black"}`}}>
                        <ListItemIcon style={{color:`${props1.location['pathname']==route['path'] && "white"}`}}>{route['icon']}</ListItemIcon>
                        <ListItemText primary={route['text']} />
                    </ListItem>
                </Link>
                <Divider />
                </Fragment>
          ))}
        </List>
      </div>
    );
    }
  
    const container = window !== undefined ? () => window().document.body : undefined;
  
    return (
        <Router history={history}>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Box flexGrow={1}>
            <Typography variant="h6" noWrap>
              Welcome {props.name}
            </Typography>
            </Box>
            <Box>
            <Button variant="contained" color="secondary" style={{float:'right'}} onClick={() => {
                setCookie('uid','',100);
                setCookie('email','',100);
                setCookie('password','',100);
                props.setIsAdmin('');
                props.setName('');
            }}>
                Logout
            </Button>
            </Box>
          </Toolbar>
        </AppBar>
        <nav className={classes.drawer} aria-label="mailbox folders">
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Hidden smUp implementation="css">
            <Drawer
              container={container}
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper,
              }}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
            >

              <Switch>
               {props.routes.map((route) => (
                    <Route exact path={route['path']} render={(props1) => <DrawerList {...props1} routes={props.routes} />} key={route['text']}/>
                ))}
                <Route path={'/'} render={(props1) => <DrawerList {...props1} routes={props.routes} />} />
              </Switch>
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              open
            >
              <Switch>
               {props.routes.map((route) => (
                    <Route exact path={route['path']} render={(props1) => <DrawerList {...props1} routes={props.routes} />} key={route['text']}/>
                ))}
                <Route path={'/'} render={(props1) => <DrawerList {...props1} routes={props.routes} />} />
              </Switch>
            </Drawer>
          </Hidden>
        </nav>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {props.routes.map((route) => (
                <Route exact path={route['path']} component={route['component']} key={route['text']}/>
            ))}
        </main>
      </div>
      </Router>
    );
    
  }

  export default ResponsiveDrawer;