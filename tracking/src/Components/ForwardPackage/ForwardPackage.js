import React, { Fragment, useContext } from 'react';

// import Barcode from "react-native-barcode-builder";


import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import LinkIcon from '@material-ui/icons/Link';

import Loading from './../Loading/Loading'

import { FirebaseContext } from '../../utils/firebase'
import 'firebase'

import {setCookie, getCookie} from '../../utils/cookie'

import PackageCard from './../PackageCard/PackageCard'

const useStyles = makeStyles({

    card: {

    }
  });

const ForwardPackage = (props) => {
    const firebase = useContext(FirebaseContext)
    const classes = useStyles();
    const [packageID, setPackageID] = React.useState('');
    const [packageDetails, setPackageDetails] = React.useState('');
    const [message, setMessage] = React.useState('');
    const [loading, setLoading] = React.useState(false);


    return(
        <Fragment>
            <FormControl>
                <InputLabel >Package ID</InputLabel>
                <Input  autoFocus={true} value={packageID} onChange={(event) => {setPackageID(event.target.value)}} onKeyDown={(event) => {
                    if (event.keyCode === 13) {
                        setLoading(true);
                        fetch('/forward_package',{
                            
                            method: 'POST',
                            headers: {
                                'Content-type': 'application/json',
                                'Accept': 'application/json'
                            },
                            body: JSON.stringify({'package_id':packageID})
                        }).then(res => res.json()).then(data=>{
                            if(data.code === 400)
                            {
                                setMessage(data.message);
                                setPackageDetails('')
                                setPackageID('');
                                setLoading(false);
                            }
                            else
                            {
                                setPackageDetails(data.package);
                                setMessage(data.message)
                                setPackageID('');
                                setLoading(false);
                            }    
                        }).catch(error=>{
                            console.log(error);
                            setLoading(false);
                        })
                    }
                }}/>
            </FormControl>
        { message !== '' && 
        
        <PackageCard message={message} packageDetails={packageDetails===''?'':{
            'Package ID': packageDetails['package_id'],
            'Weight': packageDetails['weight']+' Kgs',
            'Value': packageDetails['value'],
            'Owner Name': packageDetails['owner_name'],
            'Ph. No.': packageDetails['mob'],
            'Destination': packageDetails['path'][packageDetails['path'].length-1],
            'Expected Path': (packageDetails['path'].filter(function(place, index){return index!= packageDetails['path'].length-1 }).map(function(place){return(place+'->')}) + packageDetails['path'][packageDetails['path'].length-1])
        }}>
        </PackageCard>
        }
        { loading && <Loading />}
      </Fragment>
    );
  
}

export default ForwardPackage;