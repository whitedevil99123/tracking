import React, { useContext, Fragment } from 'react'
import './style.scss'
import { FirebaseContext } from '../../utils/firebase'
import 'firebase'
import {setCookie, getCookie} from '../../utils/cookie'

import { Router, Route, Switch} from 'react-router';
import { createBrowserHistory } from "history";

import Loading from './../Loading/Loading'

const Home = (props)=>{
  

    return(
      

      <Fragment>
      <header className="site-navbar py-3" role="banner">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
          <div className="container">
              <div className="row align-items-center">
                
                <div className="col-11 col-xl-2">
                  <h2 className="mb-0 text-white">Tracker</h2>
                </div>
                <div className="col-12 col-md-10 d-none d-xl-block">
                  <nav className="site-navigation position-relative text-right" role="navigation">
      
                    <ul className="site-menu js-clone-nav mx-auto d-none d-lg-block">
                      <li className="active"><a href="index.html">Home</a></li>
                      <li><a href="about.html">About Us</a></li>
                      <li>
                        <a href="services.html">Services</a>
                      </li>
                       <li><a href="contact.html">Contact</a></li>
  
                       <li><a href="/login">Log In or Sign Up <i className='fa fa-user-circle' style={{'font-size':'19px'}}></i></a></li>
                    </ul>
                  </nav>
                </div>
      
      
                <div className="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style={{"position": "relative", "top": "3px"}}><a href="#" className="site-menu-toggle js-menu-toggle text-white"><span className="icon-menu h3"></span></a></div>
      
                </div>
      
              </div>
            
        </header>
  
        <div className="site-blocks-cover overlay hero_bg"  >
          <div className="container">
            <div className="row align-items-center justify-content-center text-center">
    
              <div className="col-md-8" data-aos="fade-up" data-aos-delay="400">
                
    
                <h1 className="text-white font-weight-light mb-5 text-uppercase font-weight-bold">Tracking the transhipment of goods</h1>
                {/* <!-- <p><a href="./Login_v3/index.html" class="btn btn-primary py-3 px-5 text-white">Get Started!</a></p> --> */}
    
              </div>
            </div>
          </div>
        </div> 
  
  
  
  
      <div className="site-section">
        <div className="container">
          <div className="row justify-content-center mb-5">
            <div className="col-md-7 text-center border-primary">
              <h2 className="mb-0 " style={{"color": "#EF9C12"}}>What We Offer</h2>
              <p className="color-black-opacity-5">
                We provide highly specialised infrastructure facilities for handling different types of freight and act as switching points for cargo carried by deep-sea vessels operating on trans-continental trade routes</p>
            </div>
          </div>
          <div className="row align-items-stretch">
            <div className="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <div className="unit-4 d-flex">
                <div className="unit-4-icon mr-4"><i className="fa fa-plane" style={{"font-size":"48px","color":"#EF9C12"}}></i></div>
                <div>
                  <h3>Air Freight</h3>
                  <p className="color-black-opacity-5">Despite widespread hopes for a vibrant industry, for decades the air freight sector did not grow as expected and remained a very small part of total air traffic. For much of the first five post-war decades most carriers saw it as a secondary activity, although there had always been specialized cargo airlines.</p>
                  <p className="mb-0"><a href="#">Learn More</a></p>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <div className="unit-4 d-flex">
                <div className="unit-4-icon mr-4"><i className="fa fa-ship" style={{"font-size":"48px","color":"#EF9C12"}}></i></div>
                <div>
                  <h3>Ocean Freight</h3>
                  <p className="color-black-opacity-5">Freight transport is the physical process of transporting commodities and merchandise goods and cargo. The term shipping originally referred to transport by sea but in American English, it has been extended to refer to transport by land or air (International English: "carriage") as well. </p>
                  <p className="mb-0"><a href="#">Learn More</a></p>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <div className="unit-4 d-flex">
                <div class="unit-4-icon mr-4"><i className='fa fa-truck' style={{'font-size':'48px','color':'#EF9C12'}}></i></div>
                <div>
                  <h3>Ground Shipping</h3>
                  <p className="color-black-opacity-5">Ship grounding is the impact of a ship on seabed or waterway side. It may be intentional, as in beaching to land crew or cargo, and careening, for maintenance or repair, or unintentional, as in a marine accident. In accidental cases, it is commonly referred to as "running aground.</p>
                  <p className="mb-0"><a href="#">Learn More</a></p>
                </div>
              </div>
            </div>
  
          </div>
        </div>
      </div>
    
        
  
       
      
      
      
       <footer className="site-footer">
          <div className="container">
            <div className="row">
              <div className="col-md-10">
                <div className="row">
                  <div className="col-md-3">
                    <h2 className="footer-heading mb-4">Quick Links</h2>
                    <ul className="list-unstyled">
                      <li><a href="#">About Us</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">Testimonials</a></li>
                      <li><a href="#">Contact Us</a></li>
                    </ul>
                  </div>
                  <div className="col-md-3">
                    <h2 className="footer-heading mb-4">Products</h2>
                    <ul className="list-unstyled">
                      <li><a href="#">About Us</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">Testimonials</a></li>
                      <li><a href="#">Contact Us</a></li>
                    </ul>
                  </div>
                  <div className="col-md-3">
                    <h2 className="footer-heading mb-4">Features</h2>
                    <ul className="list-unstyled">
                      <li><a href="#">About Us</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">Testimonials</a></li>
                      <li><a href="#">Contact Us</a></li>
                    </ul>
                  </div>
                  <div className="col-md-3">
                    <h2 className="footer-heading mb-4">Follow Us</h2>
                    <a href="#" className="pl-0 pr-3"><span className="fa fa-facebook" style={{"font-size":"24px"}}></span></a>
                    <a href="#" className="pl-3 pr-3"><span className="fa fa-twitter" style={{"font-size":"24px"}}></span></a>
                    <a href="#" className="pl-3 pr-3"><span className="fa fa-instagram" style={{"font-size":"24px"}}></span></a>
                    <a href="#" className="pl-3 pr-3"><span className="fa fa-linkedin" style={{"font-size":"24px"}}></span></a>
                  </div>
                </div>
              </div>
    
            </div>
      
          </div>
        </footer>
        </Fragment>
    )
};

const Login = (props)=>{
  const firebase = useContext(FirebaseContext)

    
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [message, setMessage] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const login = (event) => {
    setLoading(true);
    event.preventDefault();
    firebase.auth().signInWithEmailAndPassword(email, password).then(function(res){
        console.log(res.user.uid);
        setCookie('uid',res.user.uid,100);
        setCookie('email',email,100);
        setCookie('password',password,100);
        firebase.database().ref('/network/users/'+res.user.uid).once('value').then(function(snapshot){
            if(snapshot.val().admin)
            {
              props.setIsAdmin('true')
              props.setName(snapshot.val().name)
            }
            else
            {
              props.setIsAdmin('false')
              props.setName(snapshot.val().name)
            }
            setLoading(false);
        }).catch(function(error) {
            setMessage(error.message);
            setLoading(false);
          });
    }).catch(function(error) {
      setMessage(error.message);
      setLoading(false);
    });
  }
  return(
    <Fragment>
      <div className="flex-cont hero_bg">
        
        <div className="container-1">
        
            
                <div className="row">
                <h2 style={{"text-align":"center"}}>Login with Social Media or Manually</h2>
                <div className="vl">
                    <span className="vl-innertext">or</span>
                </div>
            
                <div className="col">
                    <a href="#" className="fb btn">
                    <i className="fa fa-facebook fa-fw"></i> Login with Facebook
                    </a>
                    <a href="#" className="twitter btn">
                    <i className="fa fa-twitter fa-fw"></i> Login with Twitter
                    </a>
                    <a href="#" className="google btn"><i className="fa fa-google fa-fw">
                    </i> Login with Google+
                    </a>
                </div>
            
                <div className="col">
                    <div className="hide-md-lg">
                    <p>Or sign in manually:</p>
                    </div>
            
                    <input type="text" name="username" placeholder="Username" required onChange={(event) => {setEmail(event.target.value)}}/>
                    <input type="password" name="password" placeholder="Password" required onChange={(event) => {setPassword(event.target.value)}}/>
                    <button type="submit" value="Login" onClick={login}>Login</button>
                    {message}
                </div>
                
                </div>
            
            
        
            <div className="bottom-container">
                <div className="row">
                    <div className="col">
                    <a href="#" style={{"color":"white"}} className="btn">Sign up</a>
                    </div>
                    <div className="col">
                    <a href="#" style={{"color":"white"}} className="btn">Forgot password?</a>
                    </div>
                </div>
            </div>
        
    </div>

    
    
</div>
    { loading && <Loading />}
    </Fragment>
  )
}

const Root = (props)=>{
  const history = createBrowserHistory();
  return(
    <Router history={history}>
      <Switch>
      <Route path="/login" render={(props1) => <Login {...props}/>}/>
      <Route path="/" render={(props1) => <Home {...props}/>}/>
      </Switch>
    </Router>
  )
}

export default Root;